Role Name
=========

Installation et configuration des protections appliquées aux serveurs de LQDN.

Ce rôle va installer les paquets de sécurité suivant :

- fail2ban : limite les accès réseaux et détecte les attaques
- portsentry : surveillance des ports réseaux des machines
- rkhunter : surveillance des binaires et des modifications de la machine.
- tripwire : surveillance des binaires et des modifications de la machine.
- auditd : garder une trace de toutes les modifications de la machine.
- lynis : un outil de vérification de la sécurité de la machine.

et les configurer par défaut. Chaque service va ensuite pouvoir (re) définir des variables, pour être effectif au maximum.

Par exemple, les ports étant surveillé par portsentry, ou fail2ban, les fichiers surveillé par tripwire ou fail2ban.

Requirements
------------

Aucunes.

Role Variables
--------------

`keycloak_log_directory`

Où est le dossier dans lequel se trouve le log pour keycloak.

Dependencies
------------

Aucunes.

Example Playbook
----------------

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
